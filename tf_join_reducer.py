#!/usr/bin/env python
import sys
import string

last_document = None
cur_word = "-"
cur_times = "-"
cur_max_word = "-"

for line in sys.stdin:
    line = line.strip()
    document, word, times, max_word = line.split("\t")

    if not last_document or last_document != document:
        last_document = document
        cur_word = word
        cur_times = times
        cur_max_word = max_word
    elif document == last_document:
        word = cur_word
        times = cur_times
        max_word = cur_max_word
        print '%s\t%s\t%s\t%s' % (document, word, times, max_word)

# do not forget to output the last word if needed!
if document == last_document:
    print '%s\t%s\t%s\t%s' % (document, word, times, max_word)