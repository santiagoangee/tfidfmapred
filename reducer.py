#!/usr/bin/env python

from operator import itemgetter
import sys

current_word_document = None
current_count = 0
word_document = None

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    word, document, count = line.split('\t', 2)
    word_document = word + "\t" + document

    # convert count (currently a string) to int
    try:
        count = int(count)
    except ValueError:
        # count was not a number, so silently
        # ignore/discard this line
        continue

    # this IF-switch only works because Hadoop sorts map output
    # by key (here: word) before it is passed to the reducer
    if current_word_document == word_document:
        current_count += count
    else:
        if current_word_document:
            # write result to STDOUT
            print '%s\t%s' % (current_word_document, current_count)
        current_count = count
        current_word_document = word_document

# do not forget to output the last word if needed!
if current_word_document == word_document:
    print '%s\t%s' % (current_word_document, current_count)