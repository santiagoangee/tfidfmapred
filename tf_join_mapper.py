#!/usr/bin/env python
import sys
for line in sys.stdin:
    # Setting some defaults
    word = "-"
    document = ""
    times = "-"
    max_word = "-"

    line = line.strip()
    splits = line.split("\t")
    # TF has more columns than users
    if len(splits) == 3:
        word = splits[0]
        document = splits[1]
        times = splits[2]
    else:
        document = splits[0]
        max_word = splits[1]
    print '%s\t%s\t%s\t%s' % (document, word, times, max_word)
