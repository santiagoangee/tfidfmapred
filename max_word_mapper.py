#!/usr/bin/env python

# This code was made by Michael G. Noll
# and taken from the website http://www.michael-noll.com/tutorials/writing-an-hadoop-mapreduce-program-in-python/

import sys

# input comes from STDIN (standard input)
for line in sys.stdin:

    line = line.strip()
    # split the line into words
    pairs = line.split('\n')
    # increase counters
    #pairs = filter(None, pairs)  # fastest
    for pair in pairs:

        pair = pair.strip()
        # write the results to STDOUT (standard output);
        # what we output here will be the input for the
        # Reduce step, i.e. the input for reducer.py
        #
        # tab-delimited; the trivial word count is 1
        _, filename, count = pair.split('\t',2)

        print '%s\t%s' % (filename, count)
