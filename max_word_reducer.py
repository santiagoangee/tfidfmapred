#!/usr/bin/env python

# This code was made by Michael G. Noll
# and taken from the website http://www.michael-noll.com/tutorials/writing-an-hadoop-mapreduce-program-in-python/

from operator import itemgetter
import sys

current_filename = None
max_count = 0
filename = None

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    filename, count = line.split('\t', 1)

    # convert count (currently a string) to int
    try:
        count = int(count)
    except ValueError:
        # count was not a number, so silently
        # ignore/discard this line
        continue

    # this IF-switch only works because Hadoop sorts map output
    # by key (here: word) before it is passed to the reducer
    if current_filename == filename:
        max_count = max(max_count, count)
    else:
        if current_filename:
            # write result to STDOUT
            print '%s\t%s' % (current_filename, max_count)
        max_count = count
        current_filename = filename

# do not forget to output the last word if needed!
if current_filename == filename:
    print '%s\t%s' % (current_filename, max_count)
