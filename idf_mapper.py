#!/usr/bin/env python

import sys

# input comes from STDIN (standard input)
for line in sys.stdin:

    line = line.strip()
    # split the line into words
    pairs = line.split('\n')
    # increase counters
    #pairs = filter(None, pairs)  # fastest
    for pair in pairs:

        pair = pair.strip()
        # write the results to STDOUT (standard output);
        # what we output here will be the input for the
        # Reduce step, i.e. the input for reducer.py
        #
        # tab-delimited; the trivial word count is 1
        word, filename, count = pair.split('\t',2)

        print '%s\t%s' % (word, 1)
